﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.IO;
using System.Collections.Generic;

public class MagickBox : MonoBehaviour {

/// <summary>
/// OHJEET LÖYTYY DESKTOPISTA TXT FILESTÄ NIMELTÄ "MAGICKBOXSHORTCUTS"
/// </summary>


[MenuItem("MagickBox/Doors/Normal -> Bathroom")]
	static void NormalDoorToBathroomDoor() {
		foreach (var item in Selection.gameObjects) {
			var still = setChildActiveState (item,5, true);
			GameObject nextChild = null;
			nextChild = (GameObject)item.transform.GetChild(2).gameObject as GameObject;

			if (nextChild != null) {
				var handleLock = setChildActiveState (nextChild,3, true);


				var handle = setChildActiveState (nextChild,1, false);

			}
		}
	}



    [MenuItem("MagickBox/Doors/Bathroom -> Normal")]
	static void BathroomDoorToNormalDoor() {
		foreach (var item in Selection.gameObjects) {
			var still = setChildActiveState (item,5, false);
			GameObject nextChild = null;
			nextChild = (GameObject)item.transform.GetChild(2).gameObject as GameObject;

			if (nextChild != null) {
				var handleLock = setChildActiveState (nextChild,3, false);


				var handle = setChildActiveState (nextChild,1, true);

			}
		}
	}


    [MenuItem("MagickBox/Lightmaps/GenerateLightMapUVS")]
    static void SetGenerateLightmapUVS()
    {
        foreach (var item in Selection.objects)
        {
            string path = AssetDatabase.GetAssetPath(item);

            ModelImporter model = AssetImporter.GetAtPath(path) as ModelImporter;
            model.generateSecondaryUV = true;
        }
    }


    /* [MenuItem("MagickBox/TagManager/SetStates1 %&F1")]
     static void TestiDebugs()
     {
         setHideStates(0);
     }

     [MenuItem("MagickBox/TagManager/SetStates2 %&F2")]
     static void TestiDebugs2()
     {
         setHideStates(1);
     }

     [MenuItem("MagickBox/TagManager/SetStates3 %&F3")]
     static void TestiDebugs3()
     {
         setHideStates(2);
     }

     [MenuItem("MagickBox/TagManager/SetStates4 %&F4")]
     static void TestiDebugs4()
     {
         setHideStates(3);
     }

     [MenuItem("MagickBox/TagManager/SetStates5 %&F5")]
     static void TestiDebugs5()
     {
         setHideStates(4);
     }

     [MenuItem("MagickBox/TagManager/SetStates6 %&F6")]
     static void TestiDebugs6()
     {
         setHideStates(5);
     }

     static void setHideStates(int index)
     {
         var list = Selection.objects;


         List<GameObject> GO = new List<GameObject>();
         MCSO mcso = null;
         foreach (var item in list)
         {
             var temp = item as GameObject;

             if (temp != null)
             {
                 GO.Add(temp);
             }
             else
             {
                 mcso = item as MCSO;
             }
         }

         for (int i = 0; i < mcso.MenuItems[0].CustomButtons.HideIDS.Length; i++)
         {
             mcso.MenuItems[0].CustomButtons.HideTags[index].hideState[i] = false;
         }

         for (int i = 0; i < GO.Count; i++)
         {
             mcso.MenuItems[0].CustomButtons.HideTags[index].hideState[GO[i].GetComponent<HideTag>().ID] = true;
         }
     }*/


    [MenuItem("MagickBox/Materials/Set_4_Textures %#&m")]
	static void Set4Textures() {

		var texts = Selection.objects;
		var obj = Selection.activeGameObject;
		Debug.Log (texts.Length);
		var mat = obj.GetComponent<Renderer> ().sharedMaterial;

		foreach (var item in texts) {
			string name = item.name.Substring ((item.name.LastIndexOf('_') + 1),(item.name.Length - item.name.LastIndexOf('_') - 1));
			Debug.Log (name);
			switch (name) {
			case "Normal":
				mat.SetTexture ("_BumpMap", (Texture)item);
				break;
			case "AO":
				mat.SetTexture ("_OcclusionMap", (Texture)item);
				break;
			case "MetallicSmoothness":
				mat.SetTexture ("_MetallicGlossMap", (Texture)item);
				break;
			case "AlbedoTransparency":
				mat.SetTexture ("_MainTex", (Texture)item);
				break;
			case "Emissive":
				mat.SetTexture ("_EmissionMap", (Texture)item);
				break;
			}
		}
			


	}

	[MenuItem("MagickBox/CAVEUI/Turha %#&c")]
	static void SetCameras(){

		try {
			var items = Selection.gameObjects;
			var last = items[items.Length - 1];
			for (int i = 0; i < items.Length - 1; i++) {
				items [i].GetComponent<Canvas> ().worldCamera = last.GetComponent<Camera> ();
			}
		} catch {
			Debug.Log ("First Select all the canvases then the camera");
		}

	}

	[MenuItem("MagickBox/CAVEUI/SetCameras %#&g")]
	static void SetCamera(){

		try {
			var objs = Selection.gameObjects;
			objs[0].transform.GetChild(0).GetChild(3).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(3).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(4).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(3).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(5).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(1).GetComponent<Camera>();

			objs[0].transform.GetChild(0).GetChild(3).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(3).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(4).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(3).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(0).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(3).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(1).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(1).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(2).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(2).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(3).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(5).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(4).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(6).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(5).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(7).GetComponent<Camera>();
			objs[0].transform.GetChild(0).GetChild(6).GetChild(6).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(8).GetComponent<Camera>();

			objs[0].transform.GetChild(1).GetChild(0).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(4).GetComponent<Camera>();
			objs[0].transform.GetChild(1).GetChild(1).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(4).GetComponent<Camera>();
			objs[0].transform.GetChild(1).GetChild(2).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(4).GetComponent<Camera>();
			objs[0].transform.GetChild(1).GetChild(3).GetComponent<Canvas>().worldCamera = GameObject.Find("CAVE_Huoneistostudio 1").transform.GetChild(1).GetChild(2).GetComponent<Camera>();

		} catch {
			Debug.Log ("First Select all the canvases then the camera");
		}

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/LEFT AND RIGHT &RIGHT")]
	static void SetLeftRight(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnLeft = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () - 1)).GetComponent<Button>();
		btn.selectOnRight = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () + 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;
	}

	[MenuItem("MagickBox/GameObject/AttachToFurniture %#&f")]
	static void SetParent(){
		var current = Selection.gameObjects;
		foreach (var item in current) {
			try {
				var furn = GameObject.FindWithTag ("Furniture").transform;
				item.transform.SetParent (furn);
				setToStatic (item.gameObject, true);
			} catch {
				try {
					var parent = GameObject.Find ("Furniture");
					parent.tag = "Furniture";
					item.transform.SetParent(parent.transform);
					setToStatic (item.gameObject, true);
				} catch  {
					var furn = new GameObject ("Furniture");
					furn.tag = "Furniture";
					item.transform.SetParent(furn.transform);
					setToStatic (item.gameObject, true);
				}
			}
		}
	}





    [MenuItem("MagickBox/GameObject/AttachToFixtures %#&i")]
	static void SetFixtures(){
		var current = Selection.gameObjects;
		foreach (var item in current) {
			try {
				var furn = GameObject.FindWithTag ("Fixtures").transform;
				item.transform.SetParent (furn);
				setToStatic (item.gameObject, true);
			} catch {
				try {
					var parent = GameObject.Find ("Fixtures");
					parent.tag = "Fixtures";
					item.transform.SetParent(parent.transform);
					setToStatic (item.gameObject, true);
				} catch  {
					var furn = new GameObject ("Fixtures");
					furn.tag = "Fixtures";
					item.transform.SetParent(furn.transform);
					setToStatic (item.gameObject, true);
				}
			}
		}
	}

	

	[MenuItem("MagickBox/GameObject/ToggleActive %&a")]
	static void ToggleActive(){
		var current = Selection.gameObjects;
		foreach (var item in current) {
			if (item.activeSelf)
				item.SetActive (false);
			else
				item.SetActive (true);
		}

		
	}

    [MenuItem("MagickBox/GameObject/ToggleStatic #s")]
    static void toggleStatic()
    {
        var list = Selection.gameObjects;
        foreach (var item in list)
        {
            setToStatic(item, item.isStatic);
        }
        
    }

    private static void setToStatic(GameObject obj, bool current)
    {
        obj.gameObject.isStatic = !current;
        for (int i = 0; i < obj.transform.childCount; i++)
        {
            obj.transform.GetChild(i).gameObject.isStatic = !current;
            setToStatic(obj.transform.GetChild(i).gameObject, current);

        }
    }



    [MenuItem("MagickBox/CAVEUI/Set Cave To Current Scene Camera Location %#&p")]
	static void SetCaveToCurrentSceneCameraPosition(){
		var cave = GameObject.Find ("CAVE_Huoneistostudio");

		var newRot = Camera.current.transform.rotation;
		newRot.x = 0;
		newRot.z = 0;
		cave.transform.rotation = newRot;

		var offset = cave.transform.position - cave.transform.GetChild (1).position;

		var newPos = Camera.current.transform.position + offset;
		newPos.y = 0;
		cave.transform.position = newPos;

	}





	[MenuItem("MagickBox/Inspector/ToggleLock %&c")]
	static void ToggleLock(){
		ActiveEditorTracker.sharedTracker.isLocked = !ActiveEditorTracker.sharedTracker.isLocked;

	}

	[MenuItem("MagickBox/Inspector/selectparent %&w")]
	static void selectparent(){
		var obs = Selection.gameObjects;
		Selection.activeGameObject = obs [0].transform.parent.gameObject;
	}

	[MenuItem("MagickBox/GameObject/SetToCamLoc %#&l")]
	static void SetObjLocToCam(){
		var obj = Selection.activeGameObject;
		try {
			obj.transform.position = Camera.current.transform.position + (obj.transform.position - obj.GetComponent<Renderer> ().bounds.center);
		} catch {
			try {
				obj.transform.position = Camera.current.transform.position + (obj.transform.position - obj.GetComponentInChildren<Renderer> ().bounds.center);
			} catch {
				obj.transform.position = Camera.current.transform.position;
			}
		}

		//var pos = obj.GetComponent<Renderer> ().bounds.center;

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/LEFT %LEFT")]
	static void SetLeft(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnLeft = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () - 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/RIGHT %RIGHT")]
	static void SetRight(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnRight = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () + 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/DOWN %DOWN")]
	static void SetDown(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnDown = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () - 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/UP %UP")]
	static void SetUp(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnUp = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () + 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;

	}

	[MenuItem("MagickBox/CAVEUI/SetNavigation/UP AND DOWN &UP")]
	static void SetUpDown(){
		var current = Selection.gameObjects;
		var btn = current [0].GetComponent<Button> ().navigation;

		btn.selectOnUp = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () + 1)).GetComponent<Button>();
		btn.selectOnDown = current [0].transform.parent.GetChild ((current [0].transform.GetSiblingIndex () - 1)).GetComponent<Button>();

		current [0].GetComponent<Button> ().navigation = btn;

	}

   /* [MenuItem("MagickBox/HideTags/0 #F1")]
    static void SetHideTags0()
    {
        setHideTags(0);
    }

    [MenuItem("MagickBox/HideTags/0 #F2")]
    static void SetHideTags1()
    {
        setHideTags(1);
    }
    [MenuItem("MagickBox/HideTags/0 #F3")]
    static void SetHideTags2()
    {
        setHideTags(2);
    }
    [MenuItem("MagickBox/HideTags/0 #F4")]
    static void SetHideTags3()
    {
        setHideTags(3);
    }
    [MenuItem("MagickBox/HideTags/0 #F5")]
    static void SetHideTags4()
    {
        setHideTags(4);
    }
    [MenuItem("MagickBox/HideTags/0 #F6")]
    static void SetHideTags5()
    {
        setHideTags(5);
    }
    [MenuItem("MagickBox/HideTags/0 #F7")]
    static void SetHideTags6()
    {
        setHideTags(6);
    }*/

    /*[MenuItem("MagickBox/FloorHandler/SetFloors %F8")]
    static void SetFloorHandlerStats()
    {
        var temp = Selection.objects;
        var temp2 = Selection.gameObjects;

        FloorHandler fh = null;

        fh = temp2[0].GetComponent<FloorHandler>();

        for (int i = 0; i < fh.floorPacks.Length; i++)
        {
            if (fh.floorPacks[i].floor <= 8)
            {
                for (int y = 0; y < temp.Length; y++)
                {
                    if (temp[y].name == "2-8")
                    {
                        fh.floorPacks[i].mcso = temp[y] as MCSO;
                    }
                }

            }
            else if (fh.floorPacks[i].floor <= 12)
            {
                for (int y = 0; y < temp.Length; y++)
                {
                    if (temp[y].name == "9-14")
                    {
                        fh.floorPacks[i].mcso = temp[y] as MCSO;
                    }
                }
            }
        }
    }

	[MenuItem("MagickBox/setnewrange")]
	static void setnewrange()
	{
		var temp = Selection.objects;

		MCSO mc = temp[0] as MCSO;
		var asd = mc.MenuItems[5].TargetID[0].TargetIDS;
		long[] lol = new long[218];
		for (int i = 0; i < asd.Length; i++) {
			lol [i] = asd [i];
		}

		int last = (int)asd [asd.Length - 1];
		for (int i = asd.Length; i < asd.Length + (lol.Length - asd.Length); i++) {
			last++;
			lol [i] = last;

		}
		mc.MenuItems [5].TargetID [0].TargetIDS = lol;
	}*/

    /*[MenuItem("MagickBox/HideTags/0 &F1")]
    static void SetIDTags()
    {
        setIDTags(0);
    }

    [MenuItem("MagickBox/HideTags/0 &F2")]
    static void SetIDTags1()
    {
        setIDTags(1);
    }

    [MenuItem("MagickBox/HideTags/0 &F3")]
    static void SetIDTags2()
    {
        setIDTags(2);
    }

    [MenuItem("MagickBox/HideTags/0 &F4")]
    static void SetIDTags3()
    {
        setIDTags(3);
    }

    [MenuItem("MagickBox/HideTags/0 &F5")]
    static void SetIDTags4()
    {
        setIDTags(4);
    }

    [MenuItem("MagickBox/HideTags/0 &F6")]
    static void SetIDTags5()
    {
        setIDTags(5);
    }

    [MenuItem("MagickBox/HideTags/0 &F7")]
    static void SetIDTags6()
    {
        setIDTags(6);
    }
    [MenuItem("MagickBox/HideTags/0 &F8")]
    static void SetIDTags7()
    {
        setIDTags(7);
    }
    [MenuItem("MagickBox/HideTags/0 &F9")]
    static void SetIDTags8()
    {
        setIDTags(8);
    }

    [MenuItem("MagickBox/HideTags/0 &F10")]
    static void SetIDTags9()
    {
        setIDTags(9);
    }

    [MenuItem("MagickBox/HideTags/0 &F11")]
    static void SetIDTags10()
    {
        setIDTags(10);
    }

	[MenuItem("MagickBox/HideTags/0 &F12")]
	static void SetIDTags11()
	{
		setIDTags(11);
	}*/

	[MenuItem("MagickBox/Prefabs/BreakPrefab &%b")]
	static void BreakPrefab()
	{
		PrefabUtility.DisconnectPrefabInstance(Selection.gameObjects[0]);
	}


    /*static void setIDTags(int type)
    {
        List<HideTagList> tags = new List<HideTagList>();
        tags.Add(new HideTagList(new long[] { 0, 1, 2, 51 }));
        tags.Add(new HideTagList(new long[] { 3,4,5 , 50}));
        tags.Add(new HideTagList(new long[] { 6,49,237 }));
        tags.Add(new HideTagList(new long[] { 7,8,9 }));
        tags.Add(new HideTagList(new long[] { 10 }));
        tags.Add(new HideTagList(new long[] { 46 , 53}));
        tags.Add(new HideTagList(new long[] { 47 , 91}));


        tags.Add(new HideTagList(newRange(11, 44)));
        tags.Add(new HideTagList(new long[] { 45, 52}));
        tags.Add(new HideTagList(new long[] { 48, 236 }));
        tags.Add(new HideTagList(newRange(54, 90)));
		tags.Add(new HideTagList(newRange(92, 235)));
        doIDTags(tags[type]);
    }*/

    static long[] newRange(int start, int end)
    {
        long[] temp = new long[end - start + 1];
        for (int i = 0; i < temp.Length; i++)
        {
            temp[i] = i + start;
        }
        return temp;
    }

    /*static void doIDTags(HideTagList list)
    {
        var go = Selection.gameObjects;
        for (int i = 0; i < go.Length; i++)
        {
            try
            {
                go[i].transform.GetComponent<IDTag>().ID = list.tagList[i]; 
            }
            catch 
            {
                go[i].gameObject.AddComponent<IDTag>();
                go[i].transform.GetComponent<IDTag>().ID = list.tagList[i];
            }
            
        }
    }

    static void setHideTags(int type)
    {
        List<HideTagList> tags = new List<HideTagList>();
        tags.Add(new HideTagList(new long[] { 0, 13, 14, 15 }));
        tags.Add(new HideTagList(new long[] { 1, 20, 4, 16 }));
        tags.Add(new HideTagList(new long[] { 6, 7, 17 }));
        tags.Add(new HideTagList(new long[] { 9, 18, 11 }));
        tags.Add(new HideTagList(new long[] { 5, 8, 19 }));
        tags.Add(new HideTagList(new long[] { 10, 3, 21 }));
        tags.Add(new HideTagList(new long[] { 12, 22, 23 }));


        doHideTags(tags[type]);
    }

    static void doHideTags(HideTagList list)
    {
        var go = Selection.gameObjects;
        for (int i = 0; i < go.Length; i++)
        {
            try
            {
                go[i].transform.GetComponent<HideTag>().ID = list.tagList[i];
            }
            catch
            {
                go[i].gameObject.AddComponent<HideTag>();
                go[i].transform.GetComponent<HideTag>().ID = list.tagList[i];
            }
        }
    }*/

    private static bool setChildActiveState(GameObject root, int index, bool state) {
		var newObj = root.transform.GetChild(index);
		if (newObj != null) {
			newObj.gameObject.SetActive (state);
			return true;
		} else
			return false;

	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


}

/*public class HideTagList
{
	public long[] tagList;

	public HideTagList(long[] tagList)
	{
		this.tagList = tagList;
	}
}*/

#endif

